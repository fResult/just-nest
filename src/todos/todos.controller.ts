import { Controller, Get, Req } from '@nestjs/common';
import { Request} from 'express'

@Controller('todos')
export class TodosController {
  @Get()
  findAll(@Req() req: Request): String {
    return 'This request return all Todo list'
  }
}
