import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

const PORT = 8000
async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  await app.listen(PORT);
}

bootstrap().then(() => console.log(`Sever is started at port ${PORT}`));
